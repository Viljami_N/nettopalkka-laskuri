<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nettopalkka</title>
</head>
<body>
    <h3>Nettopalkka</h3>
    <?php
    $palkka = filter_input(INPUT_POST,'palkka',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $pidatys = filter_input(INPUT_POST,'pidatys',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $elake = filter_input(INPUT_POST,'elake',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $vakuus = filter_input(INPUT_POST,'vakuus',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $npalkka = $palkka - ($palkka * ($pidatys / 100)) - ($palkka * ($elake / 100)) - ($palkka * ($vakuus / 100));
    $mpidatys = $palkka * ($pidatys / 100);
    $melake = $palkka * ($elake / 100);
    $mvakuus = $palkka * ($vakuus / 100);
    printf("<p> Nettopalkkasi on %.2f €</p>",$npalkka);
    printf("<p> Ennakkopidätyksesi on %.2f €</p>",$mpidatys);
    printf("<p> Työeläkemaksusi on %.2f €</p>",$melake);
    printf("<p> Työttömyysvakuutesi on %.2f €</p>",$mvakuus);
    ?>
    <a href="index.html">Laske uudelleen</a>

</body>
</html>